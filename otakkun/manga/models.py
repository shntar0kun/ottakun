from django.db import models

# Create your models here.

class Lang(models.Model):
    lang = models.CharField(max_length=200, null = True, verbose_name=u"Язык")
    def __str__(self):
        return self.lang

    class Meta:
        verbose_name_plural = u'Языки'
        verbose_name = u"Язык"

class TagType(models.Model):
    '''
    Genre, Author, Rating, Year, Status and others.
    '''
    type = models.CharField(max_length=200, null = True, verbose_name=u"Тип тега")
    def __str__(self):
        return self.type

    class Meta:
        verbose_name_plural = u'Типы тегов'
        verbose_name = u"Тип тега"

class Tag(models.Model):
    tag = models.CharField(max_length=200, null = True, verbose_name=u"Тег")
    type = models.ForeignKey(TagType, verbose_name=u"Тип тега")
    thumbnail = models.ImageField(null = True)
    description = models.TextField(null = True, verbose_name=u"Описание тега")
    def __str__(self):
        return self.tag

    class Meta:
        verbose_name_plural = u'Теги'
        verbose_name = u"Тег"

class TranStat(models.Model):
    status = models.CharField(max_length=200, null = True)
    def __str__(self):
        return self.status
    class Meta:
        verbose_name_plural = u'Статусы переводов'
        verbose_name = u"Статус перевода"

class Manga(models.Model):
    name = models.CharField(max_length=200, null = True, verbose_name=u"Название")
    description = models.TextField(null = True, verbose_name=u"Описание")
    volumes = models.IntegerField(null = True, verbose_name=u"Количество томов")
    translation_status = models.ForeignKey(TranStat)
    lang = models.ForeignKey(Lang, verbose_name=u"Язык")
    tag = models.ManyToManyField(Tag, verbose_name=u"Теги")
    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = u'Манга'
        verbose_name = u"Название"

class Volume(models.Model):
    manga = models.ForeignKey(Manga, verbose_name=u"Манга")
    number = models.IntegerField(null = True, verbose_name=u"Номер тома")
    name = models.CharField(max_length=200, null = True, verbose_name=u"Название тома")
    thumbnail = models.ImageField(null = True)
    def __str__(self):
        return str(self.number)

    class Meta:
        verbose_name_plural = u'Тома манги'

class Chapter(models.Model):
    volume = models.ForeignKey(Volume, verbose_name=u"Том")
    number = models.IntegerField(null = True, verbose_name=u"Номер главы")
    name = models.CharField(max_length=200, null = True, verbose_name=u"Название главы")
    thumbnail = models.ImageField(null = True)
    pub_date = models.DateField(null = True, verbose_name=u"Дата создания")
    def __str__(self):
        return str(self.number)

    def manga_name(self):
        return self.volume.manga.name

    class Meta:
        verbose_name_plural = u'Главы манги'

class Page(models.Model):
    charter = models.ForeignKey(Chapter, verbose_name=u"Глава")
    number = models.IntegerField(null = True, verbose_name=u"Номер страницы")
    img = models.ImageField()
    def __str__(self):
        return str(self.number)

    class Meta:
        verbose_name_plural = u'Страницы манги'



