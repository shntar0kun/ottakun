# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-12 08:03
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Chapter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField(null=True, verbose_name='Номер главы')),
                ('name', models.CharField(max_length=200, null=True, verbose_name='Название главы')),
                ('thumbnail', models.ImageField(null=True, upload_to='')),
                ('pub_date', models.DateField(null=True, verbose_name='Дата создания')),
            ],
            options={
                'verbose_name_plural': 'Главы манги',
            },
        ),
        migrations.CreateModel(
            name='Lang',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lang', models.CharField(max_length=200, null=True, verbose_name='Язык')),
            ],
            options={
                'verbose_name': 'Язык',
                'verbose_name_plural': 'Языки',
            },
        ),
        migrations.CreateModel(
            name='Manga',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, null=True, verbose_name='Название')),
                ('description', models.TextField(null=True, verbose_name='Описание')),
                ('volumes', models.IntegerField(null=True, verbose_name='Количество томов')),
                ('lang', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manga.Lang', verbose_name='Язык')),
            ],
            options={
                'verbose_name': 'Название',
                'verbose_name_plural': 'Манга',
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField(null=True, verbose_name='Номер страницы')),
                ('img', models.ImageField(upload_to='')),
                ('charter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manga.Chapter', verbose_name='Глава')),
            ],
            options={
                'verbose_name_plural': 'Страницы манги',
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tag', models.CharField(max_length=200, null=True, verbose_name='Тег')),
                ('thumbnail', models.ImageField(null=True, upload_to='')),
                ('description', models.TextField(null=True, verbose_name='Описание тега')),
            ],
            options={
                'verbose_name': 'Тег',
                'verbose_name_plural': 'Теги',
            },
        ),
        migrations.CreateModel(
            name='TagType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(max_length=200, null=True, verbose_name='Тип тега')),
            ],
            options={
                'verbose_name': 'Тип тега',
                'verbose_name_plural': 'Типы тегов',
            },
        ),
        migrations.CreateModel(
            name='TranStat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(max_length=200, null=True)),
            ],
            options={
                'verbose_name': 'Статус перевода',
                'verbose_name_plural': 'Статусы переводов',
            },
        ),
        migrations.CreateModel(
            name='Volume',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField(null=True, verbose_name='Номер тома')),
                ('name', models.CharField(max_length=200, null=True, verbose_name='Название тома')),
                ('thumbnail', models.ImageField(null=True, upload_to='')),
                ('manga', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manga.Manga', verbose_name='Манга')),
            ],
            options={
                'verbose_name_plural': 'Тома манги',
            },
        ),
        migrations.AddField(
            model_name='tag',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manga.TagType', verbose_name='Тип тега'),
        ),
        migrations.AddField(
            model_name='manga',
            name='tag',
            field=models.ManyToManyField(to='manga.Tag', verbose_name='Теги'),
        ),
        migrations.AddField(
            model_name='manga',
            name='translation_status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manga.TranStat'),
        ),
        migrations.AddField(
            model_name='chapter',
            name='volume',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manga.Volume', verbose_name='Том'),
        ),
    ]
