from django.contrib import admin
from .models import Lang, Manga, TagType, Tag, Volume, Chapter, Page
# Register your models here.

class PageInline(admin.TabularInline):
    model = Page

@admin.register(Manga)
class MangaAdmin(admin.ModelAdmin):
    list_display = ('name', 'lang')

@admin.register(Volume)
class VolumeAdmin(admin.ModelAdmin):
    list_display = ('manga', 'number', 'name')

@admin.register(Chapter)
class ChapterAdmin(admin.ModelAdmin):
    inlines = [PageInline]
    list_display = ('manga_name' ,'volume', 'number', 'name')

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('tag', 'type')

admin.site.register(TagType)
admin.site.register(Lang)